<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\UserRole;
use App\Param;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = new Role();
        $role->name = Param::ROLE_ADMIN;
        $role->save();

        $role = new Role();
        $role->name = Param::ROLE_REPORTER;
        $role->save();
    }
}
