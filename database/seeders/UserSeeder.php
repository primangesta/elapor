<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use App\Models\UserRole;
use App\Param;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->username = 'superadmin';
        $user->password = bcrypt('superadmin!!');
        $user->save();

        $role = new Role();
        $role->name = Param::ROLE_SUPERADMIN;
        $role->save();

        $userRole = new UserRole();
        $userRole->username = 'superadmin';
        $userRole->role_id = $role->id;
        $userRole->save();
    }
}
