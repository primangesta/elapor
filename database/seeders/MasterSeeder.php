<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\OfficeUnit;
use App\Models\TicketType;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class MasterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $type = new TicketType();
        $type->name = 'Pelayanan';
        $type->save();

        $type = new TicketType();
        $type->name = 'Pengaduan';
        $type->save();

        $category = new Category();
        $category->name = 'Fasilitas Umum';
        $category->save();

        $category = new Category();
        $category->name = 'Pelecehan Seksual';
        $category->save();

        $category = new Category();
        $category->name = 'Grativikasi';
        $category->save();

        $unit = new OfficeUnit();
        $unit->name = 'DPSI';
        $unit->save();

        $unit = new OfficeUnit();
        $unit->name = 'SDM';
        $unit->save();

        $unit = new OfficeUnit();
        $unit->name = 'SarPras';
        $unit->save();
    }
}
