<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});

Auth::routes();

Route::get('/dashboard', [\App\Http\Controllers\Web\HomeController::class, 'index'])->name('dashboard');
Route::get('/home', [\App\Http\Controllers\Web\HomeController::class, 'home'])->name('home');
Route::get('/home/{id}', [\App\Http\Controllers\Web\HomeController::class, 'show'])->name('home.show');
Route::post('/home/{id}/send', [\App\Http\Controllers\Web\HomeController::class, 'sendMessage'])->name('home.send');
Route::post('/home/{id}/update', [\App\Http\Controllers\Web\HomeController::class, 'updateStatus'])->name('home.update');
Route::post('/home/{id}/designation', [\App\Http\Controllers\Web\HomeController::class, 'designationUnit'])->name('home.designation');

Route::get('/category', [\App\Http\Controllers\Web\CategoryController::class, 'index'])->name('category.index');
Route::post('/category', [\App\Http\Controllers\Web\CategoryController::class, 'store'])->name('category.store');
Route::post('/category/{id}', [\App\Http\Controllers\Web\CategoryController::class, 'update'])->name('category.update');
Route::delete('/category/{id}', [\App\Http\Controllers\Web\CategoryController::class, 'destroy'])->name('category.delete');

Route::get('/unit', [\App\Http\Controllers\Web\UnitController::class, 'index'])->name('unit.index');
Route::post('/unit', [\App\Http\Controllers\Web\UnitController::class, 'store'])->name('unit.store');
Route::post('/unit/{id}', [\App\Http\Controllers\Web\UnitController::class, 'update'])->name('unit.update');
Route::delete('/unit/{id}', [\App\Http\Controllers\Web\UnitController::class, 'destroy'])->name('unit.delete');

Route::get('/user', [\App\Http\Controllers\Web\UserController::class, 'index'])->name('user.index');
Route::post('/user', [\App\Http\Controllers\Web\UserController::class, 'store'])->name('user.store');
Route::post('/user/{id}', [\App\Http\Controllers\Web\UserController::class, 'update'])->name('user.update');
Route::delete('/user/{id}', [\App\Http\Controllers\Web\UserController::class, 'destroy'])->name('user.delete');
