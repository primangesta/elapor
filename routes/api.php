<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\DesignationController;
use App\Http\Controllers\OfficeUnitController;
use App\Http\Controllers\TicketController;
use App\Http\Controllers\TicketTypeController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/questions', [UserController::class, 'getQuestions'])->name('questions.index');

Route::group(['middleware' => 'api', 'prefix' => 'auth'], function ($router) {
    Route::post('register', [AuthController::class, 'register']);
    Route::post('login', [AuthController::class, 'login']);
    Route::post('logout', [AuthController::class, 'logout']);
    Route::post('refresh', [AuthController::class, 'refresh']);
    Route::post('me', [AuthController::class, 'me']);
    Route::post('forgot', [AuthController::class, 'forgot']);
});

Route::group(['middleware' => ['auth']], function ($router) {
    Route::get('users', [UserController::class, 'index'])->name('users.index');
    Route::post('users', [UserController::class, 'store'])->name('users.store');
    Route::get('users/{id}', [UserController::class, 'show'])->name('users.show');
    Route::post('users/{id}', [UserController::class, 'update'])->name('users.update');
    Route::delete('users/{id}', [UserController::class, 'destroy'])->name('users.destroy');

    Route::get('categories', [CategoryController::class, 'index'])->name('categories.index');
    Route::post('categories', [CategoryController::class, 'store'])->name('categories.store');
    Route::get('categories/{id}', [CategoryController::class, 'show'])->name('categories.show');
    Route::post('categories/{id}', [CategoryController::class, 'update'])->name('categories.update');
    Route::delete('categories/{id}', [CategoryController::class, 'destroy'])->name('categories.destroy');

    Route::get('office_units', [OfficeUnitController::class, 'index'])->name('office_units.index');
    Route::post('office_units', [OfficeUnitController::class, 'store'])->name('office_units.store');
    Route::get('office_units/{id}', [OfficeUnitController::class, 'show'])->name('office_units.show');
    Route::post('office_units/{id}', [OfficeUnitController::class, 'update'])->name('office_units.update');
    Route::delete('office_units/{id}', [OfficeUnitController::class, 'destroy'])->name('office_units.destroy');

    Route::get('ticket_types', [TicketTypeController::class, 'index'])->name('ticket_types.index');
    Route::post('ticket_types', [TicketTypeController::class, 'store'])->name('ticket_types.store');
    Route::get('ticket_types/{id}', [TicketTypeController::class, 'show'])->name('ticket_types.show');
    Route::post('ticket_types/{id}', [TicketTypeController::class, 'update'])->name('ticket_types.update');
    Route::delete('ticket_types/{id}', [TicketTypeController::class, 'destroy'])->name('ticket_types.destroy');

    Route::get('tickets', [TicketController::class, 'index'])->name('tickets.index');
    Route::post('tickets', [TicketController::class, 'store'])->name('tickets.store');
    Route::post('tickets/message', [TicketController::class, 'sendMessage'])->name('tickets.send.message');
    Route::get('tickets/my', [TicketController::class, 'myTicket'])->name('tickets.my');
    Route::get('tickets/{id}', [TicketController::class, 'show'])->name('tickets.show');

    Route::get('designations', [DesignationController::class, 'index'])->name('designations.index');
    Route::post('designations', [DesignationController::class, 'store'])->name('designations.store');
    Route::get('designations/{id}', [DesignationController::class, 'show'])->name('designations.show');
});



