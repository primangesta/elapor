<?php

namespace App;

use App\Models\Role;
use App\Models\UserRole;
use Illuminate\Support\Facades\Auth;
use JeroenNoten\LaravelAdminLte\Menu\Filters\FilterInterface;

class MenuFilter implements FilterInterface
{
    public function transform($item)
    {
        $username = auth::guard('web')->user()->username;
        $userRole = UserRole::query()
            ->where('username', $username)
            ->first();

        $role = Role::find($userRole->role_id);
        if (array_key_exists('url', $item)) {
            if ($item['url'] != 'home') {
                if ($role->name == Param::ROLE_ADMIN) {
                    $item['restricted'] = true;
                }
            }
        }

        return $item;
    }
}
