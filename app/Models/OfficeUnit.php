<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Model;

class OfficeUnit extends Model
{
    use HasUuids;

    public function parent()
    {
        return $this->belongsTo(OfficeUnit::class, 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(OfficeUnit::class, 'parent_id');
    }

    public function descendants()
    {
        return $this->children()->with('descendants');
    }

    public function tickets()
    {
        return $this->hasMany(Ticket::class);
    }
}
