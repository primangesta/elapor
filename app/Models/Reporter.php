<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Model;

class Reporter extends Model
{
    use HasUuids;

    public function tickets()
    {
        return $this->hasMany(Ticket::class, 'id', 'ticket_id');
    }
}
