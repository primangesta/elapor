<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Model;

class UserOffice extends Model
{
    use HasUuids;

    public function office()
    {
        return $this->belongsTo(OfficeUnit::class, 'office_unit_id', 'id');
    }
}
