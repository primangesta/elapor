<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Model;

class Designation extends Model
{
    use HasUuids;

    public function ticket()
    {
        return $this->belongsTo(Ticket::class);
    }

    public function office_unit()
    {
        return $this->belongsTo(OfficeUnit::class);
    }
}
