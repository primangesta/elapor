<?php

namespace App;

class Param
{
    public const QUESTION_1 = 'Nama ibu kandung?';
    public const QUESTION_2 = 'Nama hewan peliharaan?';
    public const QUESTION_3 = 'Nama sahabat pertama?';
    public const QUESTION_4 = 'Nama guru favorit?';
    public const QUESTION_5 = 'Nama kota kelahiran?';

    public const ROLE_SUPERADMIN = 'superadmin';
    public const ROLE_ADMIN = 'admin';
    public const ROLE_REPORTER = 'reporter';
}
