<?php

namespace App\Http\Controllers;

use App\Http\Responses\ApiResponse;
use App\Models\Attachment;
use App\Models\Reporter;
use App\Models\Message;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\ValidationException;

class MessageController extends Controller
{
    public function index(Request $request, $id)
    {
        $data = Message::query()
            ->where('ticket_id', $id)
            ->with(['ticket'])
            ->get();

        return ApiResponse::success($data);
    }


}
