<?php

namespace App\Http\Controllers;

use App\Http\Responses\ApiResponse;
use App\Models\TicketType;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class TicketTypeController extends Controller
{
    public function index()
    {
        $data = TicketType::all();

        return ApiResponse::success($data);
    }

    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $request->validate([
                'name' => 'required'
            ]);

            $data = new TicketType();
            $data->name = $request->name;
            $data->save();

            DB::commit();

            return ApiResponse::success($data);
        } catch (ValidationException $e) {
            DB::rollBack();

            return ApiResponse::error($e->errors());
        } catch (Exception $exception) {
            DB::rollBack();

            return ApiResponse::error($exception->getMessage());
        }
    }

    public function show($id)
    {
        $data = TicketType::query()
            ->where('id', $id)
            ->first();

        if ($data) {
            return ApiResponse::success($data);
        } else {
            return ApiResponse::error();
        }
    }

    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $request->validate([
                'name' => 'required'
            ]);

            $data = TicketType::query()
                ->where('id', $id)
                ->first();

            if (!$data) {
                return ApiResponse::error();
            }

            $data->name = $request->name;
            $data->save();

            DB::commit();

            return ApiResponse::success($data);
        } catch (ValidationException $e) {
            DB::rollBack();

            return ApiResponse::error($e->errors());
        } catch (Exception $exception) {
            DB::rollBack();

            return ApiResponse::error($exception->getMessage());
        }
    }

    public function destroy($id)
    {
        $data = TicketType::find($id);
        $data->delete();

        if ($data) {
            return ApiResponse::success($data);
        } else {
            return ApiResponse::error();
        }
    }
}
