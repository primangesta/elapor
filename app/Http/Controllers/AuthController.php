<?php

namespace App\Http\Controllers;

use App\Http\Responses\ApiResponse;
use App\Models\RecoveryQuestion;
use App\Models\Role;
use App\Models\User;
use App\Models\UserRole;
use App\Param;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'register', 'forgot']]);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {
        $credentials = request(['username', 'password']);

        if (!$token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Login Failed'], 401);
        }

        return $this->respondWithToken($token);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(auth()->user());
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }

    public function register(Request $request)
    {
        DB::beginTransaction();
        try {
            $request->validate([
                'username' => 'required',
                'password' => 'required'
            ]);

            $data = new User();
            $data->username = $request->username;
            $data->password = bcrypt($request->password);
            $data->save();

            $role = Role::query()
                ->where('name', Param::ROLE_REPORTER)
                ->first();

            if ($role) {
                $userRole = new UserRole();
                $userRole->username = $data->username;
                $userRole->role_id = $role->id;
                $userRole->save();
            }

            foreach ($request->recovery_questions as $item) {
                $recovery = new RecoveryQuestion();
                $recovery->user_id = $data->id;
                $recovery->question = $item['question'];
                $recovery->answer = $item['answer'];
                $recovery->save();
            }

            DB::commit();

            return ApiResponse::success($data);
        } catch (ValidationException $e) {
            DB::rollBack();

            return ApiResponse::error($e->errors());
        } catch (Exception $exception) {
            DB::rollBack();

            return ApiResponse::error($exception->getMessage());
        }
    }

    public function forgot(Request $request)
    {
        DB::beginTransaction();
        try {

            $request->validate([
                'username' => 'required',
                'password' => 'required',
                'recovery_questions' => 'required'
            ]);

            $data = User::query()
                ->where('username', $request->username)
                ->first();

            if ($data) {
                foreach ($request->recovery_questions as $item) {
                    $checkRecovery = $data->recoveryQuestions()->where('question', $item['question'])->first();

                    if (!$checkRecovery) {
                        return ApiResponse::error('recovery question not found');
                    } else {
                        if ($checkRecovery->answer != $item['answer']) {
                            return ApiResponse::error('wrong recovery answer');
                        }
                    }
                }
            } else {
                return ApiResponse::error('username not found');
            }

            $data->password = bcrypt($request->password);
            $data->save();

            DB::commit();

            return ApiResponse::success($data);
        } catch (ValidationException $e) {
            DB::rollBack();

            return ApiResponse::error($e->errors());
        } catch (Exception $exception) {
            DB::rollBack();

            return ApiResponse::error($exception->getMessage());
        }
    }
}
