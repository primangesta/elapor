<?php

namespace App\Http\Controllers;

use App\Http\Responses\ApiResponse;
use App\Models\RecoveryQuestion;
use App\Models\User;
use App\Param;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class UserController extends Controller
{
    public function index()
    {
        $data = User::all();

        return ApiResponse::success($data);
    }

    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $request->validate([
                'username' => 'required',
                'password' => 'required'
            ]);

            $data = new User();
            $data->username = $request->username;
            $data->password = bcrypt($request->password);
            $data->save();

            foreach ($request->recovery_questions as $item) {
                $recovery = new RecoveryQuestion();
                $recovery->user_id = $data->id;
                $recovery->question = $item['question'];
                $recovery->answer = $item['answer'];
                $recovery->save();
            }

            DB::commit();

            return ApiResponse::success($data);
        } catch (ValidationException $e) {
            DB::rollBack();

            return ApiResponse::error($e->errors());
        } catch (Exception $exception) {
            DB::rollBack();

            return ApiResponse::error($exception->getMessage());
        }
    }

    public function show($id)
    {
        $data = User::query()
            ->where('id', $id)
            ->first();

        if ($data) {
            return ApiResponse::success($data);
        } else {
            return ApiResponse::error();
        }
    }

    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $request->validate([
                'username' => 'required',
                'password' => 'required'
            ]);

            $data = User::query()
                ->where('id', $id)
                ->first();

            if (!$data) {
                return ApiResponse::error();
            }

            $data->password = bcrypt($request->password);
            $data->save();

            foreach ($request->recovery_questions as $item) {
                $recovery = new RecoveryQuestion();
                $recovery->user_id = $data->id;
                $recovery->question = $item['question'];
                $recovery->answer = $item['answer'];
                $recovery->save();
            }

            DB::commit();

            return ApiResponse::success($data);
        } catch (ValidationException $e) {
            DB::rollBack();

            return ApiResponse::error($e->errors());
        } catch (Exception $exception) {
            DB::rollBack();

            return ApiResponse::error($exception->getMessage());
        }
    }

    public function destroy($id)
    {
        $data = User::query()
            ->where('id', $id)
            ->delete();

        if ($data) {
            return ApiResponse::success($data);
        } else {
            return ApiResponse::error();
        }
    }

    public function getQuestions()
    {
        $data = [
            Param::QUESTION_1,
            Param::QUESTION_2,
            Param::QUESTION_3,
            Param::QUESTION_4,
            Param::QUESTION_5
        ];

        return ApiResponse::success($data);
    }
}
