<?php

namespace App\Http\Controllers;

use App\Http\Responses\ApiResponse;
use App\Models\Attachment;
use App\Models\Category;
use App\Models\Designation;
use App\Models\Message;
use App\Models\Reporter;
use App\Models\Ticket;
use App\Models\TicketType;
use Carbon\Carbon;
use Exception;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\ValidationException;
use JWTAuth;

class TicketController extends Controller
{
    public function index()
    {
        $data = Ticket::query()
            ->with(['category', 'ticket_type'])
            ->get();

        return ApiResponse::success($data);
    }

    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $request->validate([
                'office_id' => 'required',
                'category_id' => 'required',
                'type_id' => 'required',
                'subject' => 'required'
            ]);

            $user = auth()->user();

            $data = new Ticket();
            $data->username = $user->username;
            $data->category_id = $request->category_id;
            $data->ticket_type_id = $request->type_id;
            $data->subject = $request->subject;
            $data->ticket_date = $request->ticket_date;
            $data->office_unit_id = $request->office_id;
            $data->save();

            if ($data) {
                $designation = new Designation();
                $designation->ticket_id = $data->id;
                $designation->office_unit_id = $request->office_id;
                $designation->save();

                if ($request->has('attachments')) {
                    $no = 1;
                    foreach ($request->attachments as $file) {
                        $datetime = Carbon::now()->timestamp;
                        $image = str_replace('data:image/jpeg;base64,', '', $file);
                        $image = str_replace(' ', '+', $image);
                        $imageName = $datetime . '_' . $no . '.' . 'jpg';

                        $path = 'tickets/' . $data->id . '/' . $imageName;
                        Storage::put($path, base64_decode($image));

                        $attachment = new Attachment();
                        $attachment->ticket_id = $data->id;
                        $attachment->url = $path;
                        $attachment->save();
                        $no++;
                    }
                }
            }

            DB::commit();

            return ApiResponse::success($data);
        } catch (ValidationException $e) {
            DB::rollBack();

            return ApiResponse::error($e->errors());
        } catch (Exception $exception) {
            DB::rollBack();

            return ApiResponse::error($exception->getMessage());
        }
    }

    public function show($id)
    {
        $data = Ticket::query()
            ->where('id', $id)
            ->with(['category', 'ticket_type', 'attachments'])
            ->with('messages', function ($query) {
                $query->orderBy('created_at', 'desc')
                    ->with('role');
            })
            ->first();

        if ($data) {
            foreach ($data->attachments as $item) {
                $item->img = asset('storage/' . $item->url);
            }

            foreach ($data->messages as $item) {
                $role = $item->role->first()->name;
                $item->role_name = $role;
            }
        }

        return ApiResponse::success($data);
    }

    public function sendMessage(Request $request)
    {
        DB::beginTransaction();
        try {
            $request->validate([
                'ticket_id' => 'required',
                'message' => 'required'
            ]);

            $data = new Message();
            $data->ticket_id = $request->ticket_id;
            $data->username = auth()->user()->username;
            $data->message = $request->message;
            $data->save();

            DB::commit();

            return ApiResponse::success($data);
        } catch (ValidationException $e) {
            DB::rollBack();

            return ApiResponse::error($e->errors());
        } catch (Exception $exception) {
            DB::rollBack();

            return ApiResponse::error($exception->getMessage());
        }
    }

    public function myTicket(Request $request)
    {
        $user = auth()->user();

        if (!$user) {
            return ApiResponse::error();
        }

        $year = $request->year;
        $ticket = Ticket::query()
            ->where('username', $user->username)
            ->withWhereHas('category', function ($query) use ($request) {
                $query->when($request->category_id, function ($query) use ($request) {
                    $query->where('id', $request->category_id);
                });
            })
            ->withWhereHas('ticket_type', function ($query) use ($request) {
                $query->when($request->type_id, function ($query) use ($request) {
                    $query->where('id', $request->type_id);
                });
            })
            ->whereYear('ticket_date', $year)
            ->orderBy('created_at', $request->order_by)
            ->get();

        $types = TicketType::all();

        $result = [];
        foreach ($types as $type) {
            $chart = Category::query()
                ->whereHas('tickets', function ($query) use ($year) {
                    $query->whereYear('ticket_date', $year);
                })
                ->withCount(['tickets' => function ($query) use ($user, $year, $type) {
                    $query->where('username', $user->username)
                        ->whereYear('ticket_date', $year)
                        ->where('ticket_type_id', $type->id);
                }])
                ->get();

            $temp['name'] = $type->name;
            $temp['data'] = $chart;
            $result[] = $temp;
        }

        $data = [
            'tickets' => $ticket,
            'charts' => $result
        ];

        return ApiResponse::success($data);
    }
}
