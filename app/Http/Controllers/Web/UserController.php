<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Responses\ApiResponse;
use App\Models\OfficeUnit;
use App\Models\Role;
use App\Models\User;
use App\Models\UserOffice;
use App\Models\UserRole;
use App\Param;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;
use Illuminate\View\View;
use Session;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth.web');
    }

    public function index(Request $request)
    {
        $dataRoles = Role::query()
            ->whereNot('name', Param::ROLE_SUPERADMIN)
            ->get();

        $roles = Role::query()
            ->whereNot('name', Param::ROLE_SUPERADMIN)
            ->when($request->role_id != null, function ($query) use ($request) {
                $query->where('id', $request->role_id);
            })
            ->get();

        $data = User::query()
            ->withWhereHas('userRoles', function ($query) use ($roles) {
                $query->whereIn('role_id', $roles->pluck('id')->toArray());
            })
            ->when($request->office_id != null, function ($query) use ($request) {
                $query->withWhereHas('units', function($query) use ($request) {
                    $query->where('office_unit_id', $request->office_id);
                });
            })
            ->with('units')
            ->orderBy('username')
            ->paginate(5);

        foreach ($data as $item) {
            $item->unit = optional(optional($item->units->first())->office)->id;
        }

        $units = OfficeUnit::query()
            ->orderBy('name')
            ->get();

        return view('user.index')
            ->with('data', $data)
            ->with('units', $units)
            ->with('roles', $dataRoles);

    }

    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $request->validate([
                'office_id' => 'required',
                'username' => 'required',
                'password' => 'required'
            ]);

            $checkUsername = User::query()
                ->where('username', $request->username)
                ->first();

            if ($checkUsername) {
                DB::rollBack();
                Session::flash('error', "Nama Pengguna sudah ada");

                return redirect('user');
            }

            $data = new User();
            $data->username = $request->username;
            $data->password = bcrypt($request->password);
            $data->save();

            $role = Role::query()
                ->where('name', Param::ROLE_ADMIN)
                ->first();

            $userRole = new UserRole();
            $userRole->username = $data->username;
            $userRole->role_id = $role->id;
            $userRole->save();

            $userUnit = new UserOffice();
            $userUnit->username = $data->username;
            $userUnit->office_unit_id = $request->office_id;
            $userUnit->save();

            DB::commit();
            Session::flash('message', "Data berhasil ditambah");

            return redirect('user');
        } catch (ValidationException $e) {
            DB::rollBack();
            Session::flash('error', "Data gagal ditambah");

            return redirect('user');
        } catch (Exception $exception) {
            DB::rollBack();
            Session::flash('error', "Server mengalami gangguan");

            return redirect('user');
        }
    }

    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $request->validate([
                'office_id' => 'required',
                'username' => 'required'
            ]);

            $data = User::query()
                ->where('id', $id)
                ->first();

            if ($request->has('office_id')) {
                $userUnit = UserOffice::query()
                    ->where('username', $data->username)
                    ->first();
            }

            if (!$data) {
                return redirect('user');
            }

            $userRole = UserRole::query()
                ->where('username', $data->username)
                ->first();

            $checkUsername = User::query()
                ->where('username', $request->username)
                ->first();

            if ($checkUsername) {
                if ($checkUsername->id != $data->id) {
                    DB::rollBack();
                    Session::flash('error', "Nama Pengguna sudah ada");

                    return redirect('user');
                }
            }

            $data->username = $request->username;
            if ($request->has('password')) {
                $data->password = bcrypt($request->password);
            }
            $data->save();

            $role = Role::query()
                ->where('name', Param::ROLE_ADMIN)
                ->first();

            $userRole->username = $data->username;
            $userRole->role_id = $role->id;
            $userRole->save();

            $userUnit->username = $data->username;
            $userUnit->office_unit_id = $request->office_id;
            $userUnit->save();

            DB::commit();
            Session::flash('message', "Data berhasil diubah");

            return redirect('user');
        } catch (ValidationException $e) {
            DB::rollBack();
            Session::flash('error', "Data gagal diubah");

            return redirect('user');
        } catch (Exception $exception) {
            DB::rollBack();
            Session::flash('error', "Server mengalami gangguan");

            return redirect('user');
        }
    }

    public function destroy($id)
    {
        $data = User::find($id);

        UserRole::query()
            ->where('username', $data->username)
            ->delete();

        UserOffice::query()
            ->where('username', $data->username)
            ->delete();

        $data->delete();

        if ($data) {
            Session::flash('message', "Data berhasil dihapus");

            return redirect('user');
        } else {
            Session::flash('error', "Data gagal dihapus");

            return redirect('user');
        }
    }
}
