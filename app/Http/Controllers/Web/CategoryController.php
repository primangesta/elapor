<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Responses\ApiResponse;
use App\Models\Category;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;
use Illuminate\View\View;
use Session;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth.web');
    }

    public function index()
    {
        $data = Category::query()
            ->whereNull('parent_id')
            ->with('descendants')
            ->orderBy('name')
            ->paginate(5);

        return view('category.index')
            ->with('data', $data);
    }

    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $request->validate([
                'name' => 'required'
            ]);

            $data = new Category();
            $data->name = $request->name;
            $data->save();

            DB::commit();
            Session::flash('message', "Data berhasil ditambah");

            return redirect('category');
        } catch (ValidationException $e) {
            DB::rollBack();
            Session::flash('error', "Data gagal ditambah");

            return redirect('category');
        } catch (Exception $exception) {
            DB::rollBack();
            Session::flash('error', "Server mengalami gangguan");

            return redirect('category');
        }
    }

    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $request->validate([
                'name' => 'required|unique:categories'
            ]);

            $data = Category::query()
                ->where('id', $id)
                ->first();

            if (!$data) {
                return redirect('category');
            }

            $data->name = $request->name;
            $data->save();

            DB::commit();
            Session::flash('message', "Data berhasil diubah");

            return redirect('category');
        } catch (ValidationException $e) {
            DB::rollBack();
            Session::flash('error', "Data gagal diubah");

            return redirect('category');
        } catch (Exception $exception) {
            DB::rollBack();
            Session::flash('error', "Server mengalami gangguan");

            return redirect('category');
        }
    }

    public function destroy($id)
    {
        $data = Category::find($id);
        $data->children()->delete();
        $data->delete();

        if ($data) {
            Session::flash('message', "Data berhasil dihapus");

            return redirect('category');
        } else {
            Session::flash('error', "Data gagal dihapus");

            return redirect('category');
        }
    }
}
