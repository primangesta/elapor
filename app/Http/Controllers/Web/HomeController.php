<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Designation;
use App\Models\Message;
use App\Models\OfficeUnit;
use App\Models\Role;
use App\Models\Ticket;
use App\Models\TicketType;
use App\Models\UserOffice;
use App\Models\UserRole;
use App\Param;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth.web');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $data = Category::query()
            ->withCount(['tickets'])
            ->orderBy('tickets_count', 'DESC')
            ->get();

        $name = [];
        $total = [];
        $baseColor = [];
        for($i = 1; $i <= 6; $i++){
            $baseColor[] = $this->rand_color();
            $baseColorUnit[] = $this->rand_color();
        }
        $no = 1;
        $tempTotal = 0;
        foreach ($data as $item) {
            if ($no <= 5) {
                $name[] = $item->name;
                $total[] = $item->tickets_count;
                $color[] = $baseColor[$no - 1];
            } else {
                $tempTotal = $tempTotal + $item->tickets_count;
                if (count($data) == $no) {
                    $name[] = 'Lainnya';
                    $color[] = $baseColor[5];
                    $total[] = $tempTotal;
                }
            }
            $no++;
        }

        $data = OfficeUnit::query()
            ->withCount(['tickets'])
            ->orderBy('tickets_count', 'DESC')
            ->get();

        $nameUnit = [];
        $totalUnit = [];
        $no = 1;
        $tempTotal = 0;
        foreach ($data as $item) {
            if ($no <= 5) {
                $nameUnit[] = $item->name;
                $totalUnit[] = $item->tickets_count;
                $colorUnit[] = $baseColorUnit[$no - 1];
            } else {
                $tempTotal = $tempTotal + $item->tickets_count;
                if (count($data) == $no) {
                    $nameUnit[] = 'Lainnya';
                    $colorUnit[] = $baseColorUnit[5];
                    $totalUnit[] = $tempTotal;
                }
            }
            $no++;
        }

        return view('dashboard.index')
            ->with('name', $name)
            ->with('total', $total)
            ->with('color', $color)
            ->with('nameUnit', $nameUnit)
            ->with('totalUnit', $totalUnit)
            ->with('colorUnit', $colorUnit);
    }

    public function home(Request $request)
    {
        $username = auth('web')->user()->username;

        $user = UserRole::query()
            ->where('username', $username)
            ->first();

        $role = Role::find($user->role_id);

        $unitId = $request->office_id;
        if ($role->name == Param::ROLE_ADMIN) {
            $userUnit = UserOffice::query()
                ->where('username', $username)
                ->first();

            $units = OfficeUnit::query()
                ->where('id', $userUnit->office_unit_id)
                ->orderBy('name')
                ->get();

            $unitId = $userUnit->office_unit_id;
        } else {
            $units = OfficeUnit::query()
                ->orderBy('name')
                ->get();
        }

        $categories = Category::query()
            ->orderBy('name')
            ->get();

        $types = TicketType::query()
            ->orderBy('name')
            ->get();

        $data = Ticket::query()
            ->withWhereHas('category', function ($query) use ($request) {
                $query->when($request->category_id, function ($query) use ($request) {
                    $query->where('id', $request->category_id);
                });
            })
            ->withWhereHas('ticket_type', function ($query) use ($request) {
                $query->when($request->type_id, function ($query) use ($request) {
                    $query->where('id', $request->type_id);
                });
            })
            ->when($request->category_id, function ($query) use ($request) {
                $query->where('category_id', $request->category_id);
            })
            ->when($unitId, function ($query) use ($unitId) {
                $query->where('office_unit_id', $unitId);
            })
            ->when($request->order_by, function ($query) use ($request) {
                $query->orderBy('ticket_date', $request->order_by);
            })
            ->latest()
            ->paginate(10);

        return view('dashboard.home')
            ->with('data', $data)
            ->with('units', $units)
            ->with('categories', $categories)
            ->with('types', $types);
    }

    public function show(Request $request, $id)
    {
        $data = Ticket::query()
            ->where('id', $id)
            ->withWhereHas('category', function ($query) use ($request) {
                $query->when($request->category_id, function ($query) use ($request) {
                    $query->where('id', $request->category_id);
                });
            })
            ->withWhereHas('ticket_type', function ($query) use ($request) {
                $query->when($request->type_id, function ($query) use ($request) {
                    $query->where('id', $request->type_id);
                });
            })
            ->when($request->order_by, function ($query) use ($request) {
                $query->orderBy('ticket_date', $request->order_by);
            })
            ->first();

        $messages = Message::query()
            ->where('ticket_id', $data->id)
            ->with('role')
            ->get();

        foreach ($messages as $item) {
            $role = $item->role->first()->name;
            $item->role = $role;
        }

        $units = OfficeUnit::query()
            ->orderBy('name')
            ->get();

        $designations = Designation::query()
            ->where('ticket_id', $id)
            ->latest()
            ->get();

        return view('dashboard.show')
            ->with('data', $data)
            ->with('messages', $messages)
            ->with('units', $units)
            ->with('designations', $designations);
    }

    public function updateStatus($id)
    {
        DB::beginTransaction();
        try {
            $ticket = Ticket::find($id);
            if ($ticket) {
                $status = $ticket->is_closed;
                if ($status) {
                    $ticket->is_closed = false;
                } else {
                    $ticket->is_closed = true;
                }
                $ticket->save();
            }

            DB::commit();

            return redirect()->route('home.show', $id);
        } catch (\Exception $exception) {
            DB::rollBack();

            return redirect()->route('home.show', $id)
                ->withErrors($exception);
        }
    }

    public function designationUnit(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $ticket = Ticket::find($id);
            $ticket->office_unit_id = $request->office_id;
            $ticket->save();

            $designation = new Designation();
            $designation->ticket_id = $id;
            $designation->office_unit_id = $request->office_id;
            $designation->save();

            DB::commit();

            return redirect()->route('home.show', $id);
        } catch (\Exception $exception) {
            DB::rollBack();

            return redirect()->route('home.show', $id)
                ->withErrors($exception);
        }
    }

    public function sendMessage(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $message = new Message();
            $message->ticket_id = $id;
            $message->username = auth('web')->user()->username;
            $message->message = $request->message;
            $message->save();

            DB::commit();

            return redirect()->route('home.show', $id);
        } catch (\Exception $exception) {
            DB::rollBack();

            return redirect()->route('home.show', $id)
                ->withErrors($exception);
        }
    }

    private function rand_color()
    {
        return sprintf('#%06X', mt_rand(0, 0xFFFFFF));
    }
}
