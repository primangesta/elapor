<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Responses\ApiResponse;
use App\Models\OfficeUnit;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;
use Illuminate\View\View;
use Session;

class UnitController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth.web');
    }

    public function index()
    {
        $data = OfficeUnit::query()
            ->whereNull('parent_id')
            ->with('descendants')
            ->orderBy('name')
            ->paginate(5);

        return view('unit.index')
            ->with('data', $data);
    }

    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $request->validate([
                'name' => 'required'
            ]);

            $data = new OfficeUnit();
            $data->name = $request->name;
            $data->siat_code = $request->siat_code;
            $data->save();

            DB::commit();
            Session::flash('message', "Data berhasil ditambah");

            return redirect('unit');
        } catch (ValidationException $e) {
            DB::rollBack();
            Session::flash('error', "Data gagal ditambah");

            return redirect('unit');
        } catch (Exception $exception) {
            DB::rollBack();
            Session::flash('error', "Server mengalami gangguan");

            return redirect('unit');
        }
    }

    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $request->validate([
                'name' => 'required|unique:categories'
            ]);

            $data = OfficeUnit::query()
                ->where('id', $id)
                ->first();

            if (!$data) {
                return redirect('unit');
            }

            $data->name = $request->name;
            $data->siat_code = $request->siat_code;
            $data->save();

            DB::commit();
            Session::flash('message', "Data berhasil diubah");

            return redirect('unit');
        } catch (ValidationException $e) {
            DB::rollBack();
            Session::flash('error', "Data gagal diubah");

            return redirect('unit');
        } catch (Exception $exception) {
            DB::rollBack();
            Session::flash('error', "Server mengalami gangguan");

            return redirect('unit');
        }
    }

    public function destroy($id)
    {
        $data = OfficeUnit::find($id);
        $data->children()->delete();
        $data->delete();

        if ($data) {
            Session::flash('message', "Data berhasil dihapus");

            return redirect('unit');
        } else {
            Session::flash('error', "Data gagal dihapus");

            return redirect('unit');
        }
    }
}
