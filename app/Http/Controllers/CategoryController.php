<?php

namespace App\Http\Controllers;

use App\Http\Responses\ApiResponse;
use App\Models\Category;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class CategoryController extends Controller
{
    public function index()
    {
        $data = Category::query()
            ->whereNull('parent_id')
            ->with('descendants')
            ->get();

        return ApiResponse::success($data);
    }

    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $request->validate([
                'name' => 'required'
            ]);

            $data = new Category();
            $data->parent_id = $request->parent_id;
            $data->name = $request->name;
            $data->save();

            DB::commit();

            return ApiResponse::success($data);
        } catch (ValidationException $e) {
            DB::rollBack();

            return ApiResponse::error($e->errors());
        } catch (Exception $exception) {
            DB::rollBack();

            return ApiResponse::error($exception->getMessage());
        }
    }

    public function show($id)
    {
        $data = Category::query()
            ->where('id', $id)
            ->with('descendants')
            ->first();

        if ($data) {
            return ApiResponse::success($data);
        } else {
            return ApiResponse::error();
        }
    }

    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $request->validate([
                'name' => 'required'
            ]);

            $data = Category::query()
                ->where('id', $id)
                ->first();

            if (!$data) {
                return ApiResponse::error();
            }

            $data->parent_id = $request->parent_id;
            $data->name = $request->name;
            $data->save();

            DB::commit();

            return ApiResponse::success($data);
        } catch (ValidationException $e) {
            DB::rollBack();

            return ApiResponse::error($e->errors());
        } catch (Exception $exception) {
            DB::rollBack();

            return ApiResponse::error($exception->getMessage());
        }
    }

    public function destroy($id)
    {
        $data = Category::find($id);
        $data->children()->delete();
        $data->delete();

        if ($data) {
            return ApiResponse::success($data);
        } else {
            return ApiResponse::error();
        }
    }
}
