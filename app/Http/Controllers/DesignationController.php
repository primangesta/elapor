<?php

namespace App\Http\Controllers;

use App\Http\Responses\ApiResponse;
use App\Models\Designation;
use App\Models\TicketType;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class DesignationController extends Controller
{
    public function index()
    {
        $data = Designation::query()
            ->with(['ticket', 'office_unit'])
            ->get();

        return ApiResponse::success($data);
    }

    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $request->validate([
                'ticket_id' => 'required',
                'office_unit_id' => 'required'
            ]);

            $data = new Designation();
            $data->ticket_id = $request->ticket_id;
            $data->office_unit_id = $request->office_unit_id;
            $data->save();

            DB::commit();

            return ApiResponse::success($data);
        } catch (ValidationException $e) {
            DB::rollBack();

            return ApiResponse::error($e->errors());
        } catch (Exception $exception) {
            DB::rollBack();

            return ApiResponse::error($exception->getMessage());
        }
    }

    public function show($id)
    {
        $data = Designation::query()
            ->where('id', $id)
            ->with(['ticket', 'office_unit'])
            ->first();

        if ($data) {
            return ApiResponse::success($data);
        } else {
            return ApiResponse::error();
        }
    }

    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $request->validate([
                'ticket_id' => 'required',
                'office_unit_id' => 'required'
            ]);

            $data = Designation::query()
                ->where('id', $id)
                ->first();

            if (!$data) {
                return ApiResponse::error();
            }

            $data->ticket_id = $request->ticket_id;
            $data->office_unit_id = $request->office_unit_id;
            $data->save();

            DB::commit();

            return ApiResponse::success($data);
        } catch (ValidationException $e) {
            DB::rollBack();

            return ApiResponse::error($e->errors());
        } catch (Exception $exception) {
            DB::rollBack();

            return ApiResponse::error($exception->getMessage());
        }
    }

    public function destroy($id)
    {
        $data = Designation::find($id);
        $data->delete();

        if ($data) {
            return ApiResponse::success($data);
        } else {
            return ApiResponse::error();
        }
    }
}
