<?php

namespace App\Http\Responses;

use Symfony\Component\HttpFoundation\Response;

class ApiResponse
{
    public static function success($data = null, $message = 'Request successful', $status = Response::HTTP_OK)
    {
        return response()->json([
            'status' => 'success',
            'message' => $message,
            'data' => $data,
        ], $status);
    }

    public static function error($message = 'Error occurred', $status = Response::HTTP_BAD_REQUEST)
    {
        return response()->json([
            'status' => 'error',
            'message' => $message,
            'data' => null,
        ], $status);
    }
}
