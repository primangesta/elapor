@extends('adminlte::page')

@section('title', 'Unit')

@section('content_header')
    <h1>Pengaturan Unit</h1>
@stop

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Unit</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            @if(Session::has('message'))
                                <div class="alert alert-info alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    {{ Session::get('message') }}
                                </div>
                            @endif
                            @if(Session::has('error'))
                            <div class="alert alert-danger alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {{ Session::get('error') }}
                            </div>
                            @endif
                            <button id="add-new-btn" type="button" class="btn btn-warning float-right"
                                    style="margin-bottom: 10px" onclick="addBtn()">
                                Tambah Baru
                            </button>
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th style="width: 10px">#</th>
                                    <th>Nama</th>
                                    <th>Kode SIAT</th>
                                    <th class="text-center" style="width: 120px">Aksi</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php
                                    $no = (($data->currentPage()-1)*$data->perPage())+1;
                                @endphp
                                @foreach($data as $item)
                                    <tr>
                                        <td>{{$no++}}</td>
                                        <td>{{$item->name}}</td>
                                        <td>{{$item->siat_code}}</td>
                                        <td class="text-center">
                                            <a class="btn" onclick="editBtn({{$item}})"><i
                                                    class="fas fa-edit"></i></a>
                                            <a class="btn" onclick="deleteBtn({{$item}})"><i
                                                    class="fas fa-trash"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer clearfix">
                            <ul class="pagination pagination-sm m-0 float-right">
                                {{ $data->links() }}
                            </ul>
                        </div>


                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
        <div class="modal fade" id="modal-default">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="card card-warning">
                            <div class="card-header">
                                <h3 class="card-title" id="title_form_modal">Tambah Data Baru</h3>
                            </div>
                            <form id="formSubmit" name="formSubmit" action="" method="POST">
                                @csrf
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="name">Nama</label>
                                        <input type="text" name="name" class="form-control" id="nameFormUnit"
                                               placeholder="Nama">
                                    </div>
                                    <div class="form-group">
                                        <label for="name">Kode SIAT</label>
                                        <input type="text" name="siat_code" class="form-control" id="siatFormUnit"
                                               placeholder="Kode SIAT">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                        <button type="button" class="btn btn-warning" onclick="btnSave()">Simpan</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modal-delete">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <span>Yakin ingin menghapus?</span>
                        <form id="formDelete" name="formDelete" action="" method="POST">
                            {{ method_field('DELETE') }}
                            @csrf
                        </form>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                        <button type="button" class="btn btn-warning" onclick="btnDelete()">Hapus</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop

<script>
    function addBtn() {
        document.formSubmit.action = '{{route('unit.store')}}';
        $('#modal-default').modal('show');
    }

    function editBtn(data) {
        let id = data.id;
        let name = data.name;
        let siat_code = data.siat_code;
        let url = '{{route('unit.update', '##ID##')}}';
        url = url.replace('##ID##', id);
        document.getElementById("nameFormUnit").value = name;
        document.getElementById("siatFormUnit").value = siat_code;
        document.formSubmit.action = url;
        $('#modal-default').modal('show');
    }

    function deleteBtn(data) {
        let id = data.id;
        let url = '{{route('unit.delete', '##ID##')}}';
        url = url.replace('##ID##', id);
        document.formDelete.action = url;
        $('#modal-delete').modal('show');
    }

    function btnSave() {
        document.getElementById("formSubmit").submit();
    }

    function btnDelete() {
        document.getElementById("formDelete").submit();
    }
</script>
