@extends('adminlte::page')

@section('title', 'Beranda')

@section('content_header')
    <h1>Dashboard</h1>
@stop

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <!-- PIE CHART -->
                    <div class="card card-danger">
                        <div class="card-header">
                            <h3 class="card-title">Laporan Per Kategori</h3>
                        </div>
                        <div class="card-body">
                            <canvas id="pieChart"
                                    style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                    <!-- PIE CHART -->
                    <div class="card card-info">
                        <div class="card-header">
                            <h3 class="card-title">Laporan Per Unit</h3>
                        </div>
                        <div class="card-body">
                            <canvas id="pieChartUnit"
                                    style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
@stop
@section('js')
    <script>
        $(function () {
            var donutData = {
                labels: @json($name),
                datasets: [
                    {
                        data: @json($total),
                        backgroundColor: @json($color),
                    }
                ]
            }

            //-------------
            //- PIE CHART -
            //-------------
            // Get context with jQuery - using jQuery's .get() method.
            var pieChartCanvas = $('#pieChart').get(0).getContext('2d')
            var pieData = donutData;
            var pieOptions = {
                maintainAspectRatio: false,
                responsive: true,
            }
            //Create pie or douhnut chart
            // You can switch between pie and douhnut using the method below.
            new Chart(pieChartCanvas, {
                type: 'pie',
                data: pieData,
                options: pieOptions
            })

            var donutDataUnit = {
                labels: @json($nameUnit),
                datasets: [
                    {
                        data: @json($totalUnit),
                        backgroundColor: @json($colorUnit),
                    }
                ]
            }

            //-------------
            //- PIE CHART -
            //-------------
            // Get context with jQuery - using jQuery's .get() method.
            var pieChartCanvasUnit = $('#pieChartUnit').get(0).getContext('2d')
            var pieDataUnit = donutDataUnit;
            var pieOptionsUnit = {
                maintainAspectRatio: false,
                responsive: true,
            }
            //Create pie or douhnut chart
            // You can switch between pie and douhnut using the method below.
            new Chart(pieChartCanvasUnit, {
                type: 'pie',
                data: pieDataUnit,
                options: pieOptionsUnit
            })

        })
    </script>
@endsection


