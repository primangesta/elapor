@extends('adminlte::page')

@section('title', 'Beranda')

@section('content_header')
    <h1>Beranda</h1>
@stop

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Tiket</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <form action="{{route('home')}}">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <select class="form-control select2" name="type_id">
                                                <option value="">Pilih Jenis</option>
                                                @foreach($types as $type)
                                                    <option id="{{$type->id}}"
                                                            value="{{$type->id}}" @if(app('request')->input('type_id') == $type->id) selected @endif>{{$type->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <select class="form-control select2" name="category_id">
                                                <option value="">Pilih Kategori</option>
                                                @foreach($categories as $category)
                                                    <option id="{{$category->id}}"
                                                            value="{{$category->id}}" @if(app('request')->input('category_id') == $category->id) selected @endif>{{$category->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <select class="form-control select2" name="office_id">
                                                <option value="">Pilih Unit</option>
                                                @foreach($units as $unit)
                                                    <option id="{{$unit->id}}"
                                                            value="{{$unit->id}}" @if(app('request')->input('office_id') == $unit->id) selected @endif>{{$unit->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <button type="submit" class="btn btn-warning"
                                                style="margin-top:4px;">
                                            <i class="fas fa-filter"></i>
                                        </button>
                                        <a href="{{route('home')}}" class="btn btn-danger"
                                           style="margin-top:4px;">
                                            <i class="fas fa-sync"></i>
                                        </a>
                                    </div>
                                </div>
                            </form>
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th style="width: 10px">#</th>
                                    <th>Tipe</th>
                                    <th>Kategori</th>
                                    <th>Unit</th>
                                    <th>Deskripsi</th>
                                    <th>Status</th>
                                    <th style="width: 40px">Label</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php
                                    $no = 1;
                                @endphp
                                @foreach($data as $item)
                                <tr>
                                    <td>{{$no++}}</td>
                                    <td>{{$item->ticket_type->name}}</td>
                                    <td>{{$item->category->name}}</td>
                                    <td>{{optional($item->designation()->latest()->first()->office_unit)->name}}</td>
                                    <td>{{$item->subject}}</td>
                                    <td>{!!($item->is_closed) ? '<button class="btn btn-block btn-outline-success btn-xs disabled">Selesai</button>' : '<button class="btn btn-block btn-outline-danger btn-xs disabled">Proses</button>'!!}</td>
                                    <td><a class="btn" href="{{route('home.show', $item->id)}}"><i class="fas fa-eye"></i></a></td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer clearfix">
                            <ul class="pagination pagination-sm m-0 float-right">
                                {{ $data->appends(request()->query())->links() }}
                            </ul>
                        </div>


                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
@stop
