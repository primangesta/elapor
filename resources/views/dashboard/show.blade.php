@extends('adminlte::page')

@section('title', 'Beranda')

@section('content_header')
    <h1>Tiket Detail</h1>
@stop

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title align-middle">Tiket</h3>
                            <a href="{{route('home')}}" class="btn btn-outline-info btn-sm float-right">Kembali</a>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Tipe</th>
                                    <th>Kategori</th>
                                    <th>Unit</th>
                                    <th>Deskripsi</th>
                                    <th>Status</th>
                                    <th class="text-center">Aksi</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>{{$data->ticket_type->name}}</td>
                                    <td>{{$data->category->name}}</td>
                                    <td>{{optional($data->designation()->latest()->first()->office_unit)->name}}</td>
                                    <td>{{$data->subject}}</td>
                                    <td>{!!($data->is_closed) ? '<button class="btn btn-block btn-outline-success btn-xs disabled">Selesai</button>' : '<button class="btn btn-block btn-outline-danger btn-xs disabled">Proses</button>'!!}</td>
                                    <td class="text-center">
                                    <form method="POST" action="{{route('home.update', $data->id)}}">
                                        @csrf
                                        <a class="btn" href="#" onclick="event.preventDefault(); this.closest('form').submit();"><i class="fas fa-sync"></i></a>
                                    </form>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 flex flex-column">
                    <div class="card card-default">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Foto</label>
                                    </div>
                                    <div>
                                        <div class="filter-container p-0 row">
                                            @php
                                                $no = 1;
                                            @endphp
                                            @foreach($data->attachments as $item)
                                            <div class="col-sm-4">
                                                <a href="{{asset('storage/'.$item->url)}}" data-toggle="lightbox" data-title="Foto - {{$no}}">
                                                    <img src="{{asset('storage/'.$item->url)}}" style="max-width: 150px">
                                                </a>
                                            </div>
                                            @php
                                                $no++
                                            @endphp
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <!-- /.col -->
                            </div>
                        </div>
                        <!-- /.row -->
                    </div>
                    <div class="card card-default">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <form method="POST" action="{{route('home.designation', $data->id)}}">
                                        @csrf
                                        <div class="form-group">
                                            <label>Penunjukan Unit</label>
                                            <select class="form-control select2" name="office_id">
                                                @foreach($units as $unit)
                                                    <option @if($unit->id == $data->designation()->latest()->first()->office_unit->id) selected @endif value="{{$unit->id}}">{{$unit->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <a class="btn btn-outline-info btn-sm float-right" href="#" onclick="event.preventDefault(); this.closest('form').submit();">Ubah</a>
                                    </form>
                                </div>
                                <!-- /.col -->
                            </div>
                        </div>
                        <!-- /.row -->
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title align-middle">Riwayat Penunjukan Unit</h3>
                            <a href="{{route('home')}}" class="btn btn-outline-info btn-sm float-right">Kembali</a>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Unit</th>
                                    <th>Tanggal</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($designations as $item)
                                    <tr>
                                        <td>{{$item->office_unit->name}}</td>
                                        <td>{{$item->created_at}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card card-warning direct-chat direct-chat-warning">
                <div class="card-header">
                    <h3 class="card-title">Pesan</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <!-- Conversations are loaded here -->
                    <div class="direct-chat-messages">
                        <!-- Message. Default to the left -->
                        @foreach($messages as $item)
                        <div class="direct-chat-msg @if($item->role == \App\Param::ROLE_REPORTER) right @endif">
                            <div class="direct-chat-infos clearfix">
                                <span class="direct-chat-name @if($item->role == \App\Param::ROLE_REPORTER) float-right @else float-left @endif">{{$item->username}}</span>
                                <span class="direct-chat-timestamp @if($item->role == \App\Param::ROLE_REPORTER) float-left @else float-right @endif">{{$item->created_at}}</span>
                            </div>
                            <!-- /.direct-chat-infos -->
                            <img class="direct-chat-img" src="{{asset('assets/pp.jpg')}}"
                                 alt="message user image">
                            <!-- /.direct-chat-img -->
                            <div class="direct-chat-text">
                                {{$item->message}}
                            </div>
                            <!-- /.direct-chat-text -->
                        </div>
                        @endforeach
                    </div>
                    <!--/.direct-chat-messages-->
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    <form action="{{route('home.send', $data->id)}}" method="post">
                        @csrf
                        <div class="input-group">
                            <input type="text" name="message" placeholder="Type Message ..." class="form-control" required>
                            <span class="input-group-append">
                              <button type="submit" class="btn btn-danger">Send</button>
                            </span>
                        </div>
                    </form>
                </div>
                <!-- /.card-footer-->
            </div>
            <!--/.direct-chat -->
        </div><!-- /.container-fluid -->
    </section>
@stop

@section('js')
    <script>
        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2();

            $(document).on('click', '[data-toggle="lightbox"]', function(event) {
                event.preventDefault();
                $(this).ekkoLightbox({
                    alwaysShowClose: true
                });
            });
        });
    </script>
@stop
