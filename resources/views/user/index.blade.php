@extends('adminlte::page')

@section('title', 'Pengguna')

@section('content_header')
    <h1>Pengaturan Pengguna</h1>
@stop

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Pengguna</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            @if(Session::has('message'))
                                <div class="alert alert-info alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                                        &times;
                                    </button>
                                    {{ Session::get('message') }}
                                </div>
                            @endif
                            @if(Session::has('error'))
                                <div class="alert alert-danger alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                                        &times;
                                    </button>
                                    {{ Session::get('error') }}
                                </div>
                            @endif
                            <form action="{{route('user.index')}}">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <select class="form-control select2" name="role_id">
                                                <option value="">Pilih Role</option>
                                                @foreach($roles as $role)
                                                    <option id="{{$role->id}}"
                                                            value="{{$role->id}}"
                                                            @if(app('request')->input('role_id') == $role->id) selected @endif>{{$role->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <select class="form-control select2" name="office_id">
                                                <option value="">Pilih Unit</option>
                                                @foreach($units as $unit)
                                                    <option id="{{$unit->id}}"
                                                            value="{{$unit->id}}"
                                                            @if(app('request')->input('office_id') == $unit->id) selected @endif>{{$unit->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <button type="submit" class="btn btn-warning"
                                                style="margin-top:4px;">
                                            <i class="fas fa-filter"></i>
                                        </button>
                                        <a href="{{route('user.index')}}" class="btn btn-danger"
                                           style="margin-top:4px;">
                                            <i class="fas fa-sync"></i>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <button id="add-new-btn" type="button" class="btn btn-warning float-right"
                                                style="margin-bottom: 10px; max-height: 38px;" onclick="addBtn()">
                                            Tambah Baru
                                        </button>
                                    </div>
                                </div>
                            </form>
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th style="width: 10px">#</th>
                                    <th>Role</th>
                                    <th>Office</th>
                                    <th>Nama</th>
                                    <th class="text-center" style="width: 120px">Aksi</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php
                                    $no = (($data->currentPage()-1)*$data->perPage())+1;
                                @endphp
                                @foreach($data as $item)
                                    <tr>
                                        <td>{{$no++}}</td>
                                        <td>{{optional(optional($item->userRoles->first())->role)->name}}</td>
                                        <td>{{optional(optional($item->units->first())->office)->name}}</td>
                                        <td>{{$item->username}}</td>
                                        <td class="text-center">
                                            @if(optional(optional($item->userRoles->first())->role)->name == \App\Param::ROLE_ADMIN)
                                                <a class="btn" onclick="editBtn({{$item}})"><i
                                                        class="fas fa-edit"></i></a>
                                                <a class="btn" onclick="deleteBtn({{$item}})"><i
                                                        class="fas fa-trash"></i></a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer clearfix">
                            <ul class="pagination pagination-sm m-0 float-right">
                                {{ $data->appends(request()->query())->links() }}
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
        <div class="modal fade" id="modal-default">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="card card-warning">
                            <div class="card-header">
                                <h3 class="card-title" id="title_form_modal">Tambah Data Baru</h3>
                            </div>
                            <form id="formSubmit" name="formSubmit" action="" method="POST">
                                @csrf
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="office_id">Unit</label>
                                        <select class="form-control select2" name="office_id" required>
                                            <option value="">Pilih Unit</option>
                                            @foreach($units as $unit)
                                                <option id="{{$unit->id}}"
                                                        value="{{$unit->id}}">{{$unit->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="username">Nama Pengguna</label>
                                        <input type="text" name="username" class="form-control"
                                               id="usernameFormPengguna"
                                               placeholder="Nama Pengguna" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="password">Kata Sandi</label>
                                        <input type="password" name="password" class="form-control"
                                               id="passwordFormPengguna"
                                               placeholder="Kata Sandi" required>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                        <button type="button" class="btn btn-warning" onclick="btnSave()">Simpan</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modal-delete">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <span>Yakin ingin menghapus?</span>
                        <form id="formDelete" name="formDelete" action="" method="POST">
                            {{ method_field('DELETE') }}
                            @csrf
                        </form>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                        <button type="button" class="btn btn-warning" onclick="btnDelete()">Hapus</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop

<script>
    function addBtn() {
        document.formSubmit.action = '{{route('user.store')}}';
        $('#modal-default').modal('show');
    }

    function editBtn(data) {
        let id = data.id;
        let username = data.username;
        let unit = data.unit;
        console.log(unit);
        let url = '{{route('user.update', '##ID##')}}';
        url = url.replace('##ID##', id);
        document.getElementById("usernameFormPengguna").value = username;
        document.getElementById(unit).selected = "true";
        document.formSubmit.action = url;
        $('#modal-default').modal('show');
    }

    function deleteBtn(data) {
        let id = data.id;
        let url = '{{route('user.delete', '##ID##')}}';
        url = url.replace('##ID##', id);
        document.formDelete.action = url;
        $('#modal-delete').modal('show');
    }

    function btnSave() {
        document.getElementById("formSubmit").submit();
    }

    function btnDelete() {
        document.getElementById("formDelete").submit();
    }
</script>
